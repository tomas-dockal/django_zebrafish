from django.urls import path

from .views import MultipleModelView, RackDetail, SubstockDetail, StockDetail, FishlineDetail, Racks, Rooms, FishlinesList

urlpatterns = [
    path('', MultipleModelView.as_view()),
    path('rack/<int:pk>', RackDetail.as_view(), name='rack'),
    path('available_racks', Racks.as_view(), name='racks'),
    path('rooms', Rooms.as_view(), name='rooms'),
    path('substock/<int:pk>', SubstockDetail.as_view(), name='substock'),
    path('stock/<int:pk>', StockDetail.as_view(), name='stock'),
    path('fishline/<int:pk>', FishlineDetail.as_view(), name='fishline'),
    path('fishlines', FishlinesList.as_view(), name='fishlineslist'),

]