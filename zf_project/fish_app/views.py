from django.views.generic import TemplateView, DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Room, Rack, Position, Substock, Stock, Fishline

class MultipleModelView(TemplateView):
    template_name = 'fish_app/index.html'

    def get_context_data(self, **kwargs):
        context = super(MultipleModelView, self).get_context_data(**kwargs)
        context['rooms'] = list(Room.objects.all())
        context['racks'] = list(Rack.objects.all())
        context['positions'] = list(Position.objects.all())
        return context

class RackDetail(LoginRequiredMixin, DetailView):
    template_name = 'fish_app/rack.html'

    model = Rack

    def get_context_data(self, **kwargs):
        context = super(RackDetail, self).get_context_data(**kwargs)

        return context

class SubstockDetail(DetailView):
    template_name = 'fish_app/substock.html'

    model = Substock

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class StockDetail(DetailView):
    template_name = 'fish_app/stock.html'

    model = Stock

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class FishlineDetail(DetailView):
    template_name = 'fish_app/fishline.html'

    model = Fishline

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class Racks(ListView):
    template_name = 'fish_app/available_racks.html'

    model = Rack

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class Rooms(ListView):
    template_name = 'fish_app/rooms.html'

    model = Room

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class FishlinesList(ListView):
    template_name = 'fish_app/fishlines.html'

    model = Fishline

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context





# def login(request):
#     pass
#
# def index(request):
#     return render(request, 'zebrafish/index.html')
