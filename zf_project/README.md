### FishApp is application for managing data about zebrafish for genetic experiments. It anages data about fish stock, its loacation, counts etc.

FishApp is designed to use Python 3.7+ and Django 2.2

## clone
clone the repository 

git clone https://tomas-dockal@bitbucket.org/tomas-dockal/django_zebrafish.git

## requirements
if you have django installed on your local system, you can proceed towards installation

if you dont't have django installed, create a virtual environment and from terminal at the project's root directory (zf_project) run

    pip install -r requirements.txt

## run installation script
The script will run migrations, create new database and load it with sample data from json file

    ./runinstall

## start the local django server

    ./runserver

## start browser
application will open with your default browser at localhost, you will be asked to log in   

    ./startbrowser


